<?php

namespace app\controllers;

use yii\web\Controller;
use yii\helpers\Url;

class MovieController extends Controller
{
    public function actionView()
    {
        // Your code for fetching movie data goes here

        $movie = [
            'title' => 'Interstellar',
            'image' => Url::to('@web/uploads/interstellar.jpg', true),
            'buttonLabel' => 'Download Subtitle Now',
            'buttonUrl' => 'http://52.29.144.236:8080/index.php/s/sGbnuS2JCo70UKZ',
        ];

        return $this->render('view', [
            'movie' => $movie,
        ]);
    }
}
