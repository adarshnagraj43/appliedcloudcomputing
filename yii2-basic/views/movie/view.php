<?php

use yii\helpers\Html;

$this->title = 'Movies';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="movie-index">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <?php
        $movies = [
            [
                'name' => 'Interstellar',
                'image' => 'https://pbs.twimg.com/media/EmUeXUZVcAA5v6j.jpg',
                'buttonLabel' => 'Download Subtitle',
                'buttonUrl' => 'http://52.29.144.236:8080/index.php/s/iwsce9w1cxkgScY',
                'imageWidth' => '300px',
                'imageHeight' => '200px',
            ],
            [
                'name' => 'Mission Impossible Fallout',
                'image' => 'https://upload.wikimedia.org/wikipedia/en/f/ff/MI_%E2%80%93_Fallout.jpg',
                'buttonLabel' => 'Download Subtitle',
                'buttonUrl' => 'http://52.29.144.236:8080/index.php/s/2UZM2aB6fk7pHpE',
                'imageWidth' => '300px',
                'imageHeight' => '200px',
            ],

           [
                'name' => 'Indiana Jones',
                'image' => 'https://m.media-amazon.com/images/M/MV5BZDQxMTY3NTAtMzYwYi00Y2U3LThkYmQtOTljY2I4ZmJiZGIzXkEyXkFqcGdeQXVyMTU1NzY5NTky._V1_.jpg',
                'buttonLabel' => 'Download Subtitle',
                'buttonUrl' => 'http://52.29.144.236:8080/index.php/s/Ywd5a32nPxhuzpv',
                'imageWidth' => '300px',
                'imageHeight' => '200px',
            ],  [
                'name' => 'Iron Man',
                'image' => 'https://m.media-amazon.com/images/I/81Lh0AGGyZL._AC_UF1000,1000_QL80_.jpg',
                'buttonLabel' => 'Download Subtitle',
                'buttonUrl' => 'http://52.29.144.236:8080/index.php/s/EF3CNAloLo9a9FR',
                'imageWidth' => '300px',
                'imageHeight' => '200px',
            ],
        ];

        foreach ($movies as $movie) {
        ?>
            <div class="col-md-6">
                <div class="thumbnail">
                    <?= Html::img($movie['image'], [
                        'alt' => $movie['name'],
                        'class' => 'img-responsive',
                        'width' => $movie['imageWidth'],
                        'height' => $movie['imageHeight'],
                    ]) ?>
                    <div class="caption">
                        <h3><?= Html::encode($movie['name']) ?></h3>
                        <?= Html::a($movie['buttonLabel'], $movie['buttonUrl'], ['class' => 'btn btn-danger']) ?>
                    </div>
                </div>
            </div>
        <?php
        }
        ?>
    </div>
</div>
