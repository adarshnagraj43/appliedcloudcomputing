<?php
use yii\helpers\Html;

$this->title = 'Welcome to Yii2 Basic';
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    .site-index {
        display: grid;
        grid-template-columns: repeat(2, 1fr);
        gap: 20px;
    }

    .box {
        padding: 20px;
        border-radius: 5px;
        box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
    }

    .box.red {
        background-color: #C0C0C0 ;
        color: #000000;
    }

    .box.blue {
        background-color: #C0C0C0;
        color: #000000;
    }

    .box.green {
        background-color: #C0C0C0;
        color: #000000;
    }

    .box a {
        display: inline-block;
        margin-top: 10px;
        padding: 8px 16px;
        background-color: #481C25;
        color: #FFFFFF;
        text-decoration: none;
        border-radius: 4px;
    }
</style>

<div class="site-index">
    <div class="box red">
        <h2><?= Html::encode($this->title) ?></h2>
        <?= Html::a('Yii2 Basic', 'http://3.73.86.68:8000/', ['target' => '_blank']) ?>
    </div>

    <div class="box blue">
        <h2>Yii2 Advanced</h2>
        <?= Html::a('Yii2 Advanced', 'http://3.73.86.68/', ['target' => '_blank']) ?>
    </div>

    <div class="box green">
        <h2>OwnCloud</h2>
        <?= Html::a('OwnCloud', 'http://52.29.144.236:8080/', ['target' => '_blank']) ?>
    </div>

</div>
