# Yii2 Basic, Yii2 Advanced & OwnCloud Docker Setup

This repository provides a Docker setup for running Yii2 Basic and OwnCloud using Docker Compose.

## Prerequisites

Before running the Docker containers, make sure you have the following software installed on your machine:


- Docker
- Docker Compose

sudo snap install docker
sudo apt install docker-compose
## Instructions

1. Clone the repository:

```shell
$ git clone https://gitlab.com/adarshnagraj43/appliedcloudcomputing.git
$ cd repo

Yii2 Basic Setup
Go to the Yii2 Basic directory:

$ cd yii2-basic

Install the project dependencies:
$ composer install

Start the Yii2 Basic container:
$ docker-compose up -d yii2-basic

Access the Yii2 Basic application in your browser:
http://localhost:8080

OwnCloud Setup
Go to the OwnCloud directory:
$ cd ../owncloud

Start the OwnCloud container:
$ docker-compose up -d owncloud

Access OwnCloud in your browser:
http://localhost:8888

Configuration
Yii2 Basic:
The Yii2 Basic application files are located in the yii2-basic directory.
You can make changes to the Yii2 Basic application code by editing the files in the yii2-basic directory.
The application will be accessible at http://localhost:8080.
OwnCloud:
The OwnCloud files and data are located in the owncloud directory.
You can configure the OwnCloud settings by editing the docker-compose.yml file in the owncloud directory.
OwnCloud will be accessible at http://localhost:8888.
Clean Up
To stop and remove the Docker containers, run the following commands:


$ cd yii2-basic
$ docker-compose down

$ cd ../owncloud
$ docker-compose down

